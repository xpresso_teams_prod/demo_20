import os
import sys
import types
import numpy
import keras
import pickle
import pandas
import marshal
import seaborn
import autofeat
import warnings
import matplotlib
import tensorflow
import matplotlib.pyplot
from xgboost import XGBRegressor
from keras.models import Sequential
from sklearn.impute import SimpleImputer
from keras.layers import Dense, Dropout, LSTM
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from xpresso.ai.core.data.exploration import Explorer
from autofeat import FeatureSelector, AutoFeatRegressor
from xpresso.ai.core.data.automl import StructuredDataset
from xpresso.ai.core.data.visualization import Visualization
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from sklearn.preprocessing import MinMaxScaler, RobustScaler, QuantileTransformer
from xpresso.ai.core.data.versioning.controller_factory import VersionControllerFactory
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    train_file = pickle.load(open(f"{PICKLE_PATH}/train_file.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))


try:
    create_training_and_test_data = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/create_training_and_test_data.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    normalization = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/normalization.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    data_extraction = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/data_extraction.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    data_cleaning = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/data_cleaning.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))

## $xpr_param_component_name = data_prep
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["train_file"]
## $xpr_param_global_methods = ["create_training_and_test_data", "normalization","data_extraction","data_cleaning"]
path = "/data/turbofan"
#path = "/home/jovyan/predictive_maintenance/pipelines/pm-pipeline"
train_files = ['train_FD001.txt']
train_file = train_files[0]
def data_extraction(file):
    df = pandas.read_csv(os.path.join(path, file), sep = ' ', header = None)
    print("Shape of training data: ",df.shape)
    return df
def data_cleaning(dataframe):
    dataframe_modified = dataframe.drop(dataframe.columns[[26, 27]], axis = 1) # dropping last two NaN columns
    dataframe_modified.columns = ['id', 'cycle', 'setting1', 'setting2', 'setting3', 's1', 's2', 's3',
                     's4', 's5', 's6', 's7', 's8', 's9', 's10', 's11', 's12', 's13', 's14',
                     's15', 's16', 's17', 's18', 's19', 's20', 's21']
    
    # Calculating the RUL (Remaining Useful Life) for each id
    rul = pandas.DataFrame(dataframe_modified.groupby('id')['cycle'].max()).reset_index() # max cycles for each id
    rul.columns = ['id', 'maxCycles'] 
    train_df = dataframe_modified.merge(rul, on=['id'], how='left') 
    train_df['RUL'] = train_df['maxCycles'] - train_df['cycle']
    train_df.drop('maxCycles', axis=1, inplace=True) 
    return train_df
# Extracting data
df_initial = data_extraction(train_file)
# Removing columns with NaN and adding the RUL column
df_cleaned = data_cleaning(df_initial)
df_cleaned

try:
    pickle.dump(marshal.dumps(create_training_and_test_data.__code__), open(f"{PICKLE_PATH}/create_training_and_test_data.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(marshal.dumps(normalization.__code__), open(f"{PICKLE_PATH}/normalization.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(marshal.dumps(data_extraction.__code__), open(f"{PICKLE_PATH}/data_extraction.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(marshal.dumps(data_cleaning.__code__), open(f"{PICKLE_PATH}/data_cleaning.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))


try:
    pickle.dump(train_file, open(f"{PICKLE_PATH}/train_file.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

